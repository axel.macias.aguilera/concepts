﻿using System;
using System.Collections.Generic;

namespace Concepts
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User("Axel Macias", 24, 'M');
            user.ShowUser();
            Worker worker = new Worker("Citlalli Hernandez", 24, 'F', 12000.5);
            worker.ShowUser();
        }


        public class User
        {
            //Reference Types
            public string Name { get; set; }

            //Value Types
            public int Age { get; set; }
            public char Genere { get; set; }

            public User(string Name, int Age, char Genere)
            {
                this.Name = Name;
                this.Age = Age;
                this.Genere = Genere;
            }

           virtual public void ShowUser()
           {
                var response = new BasicResponse
                {
                    Status = "OK",
                    Messagge = $"The info about the new user is: {this.Name}, {this.Age}, {this.Genere}"
                };
                Console.WriteLine(response.SendResponse()); 
           }

            
        }


        public class Worker : User
        {
            public double Salary { get; set; }

            public Worker(string Name, int Age, char Genere, double Salary) : base(Name, Age, Genere)
            {
                this.Name = Name;
                this.Age = Age;
                this.Genere = Genere;
                this.Salary = Salary;
            }

            public override void ShowUser()
            {
                var response = new BasicResponse
                {
                    Status = "OK",
                    Messagge = $"The info about the new worker is: {this.Name}, {this.Age}, {this.Genere}, {this.Salary}"
                };
                Console.WriteLine(response.SendResponse());
            }
        }

        public struct BasicResponse
        {
            public string Status { get; set; }
            public string Messagge { get; set; }

            public string SendResponse()
            {
                return Status + " " + Messagge;
            }
        }

    }
}
